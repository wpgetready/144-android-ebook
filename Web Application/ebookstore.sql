SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


CREATE TABLE IF NOT EXISTS `es_activities` (
  `activity_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL DEFAULT '0',
  `activity` varchar(255) NOT NULL,
  `module` varchar(255) NOT NULL,
  `created_on` datetime NOT NULL,
  `deleted` tinyint(12) NOT NULL DEFAULT '0',
  PRIMARY KEY (`activity_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=86 ;

INSERT INTO `es_activities` (`activity_id`, `user_id`, `activity`, `module`, `created_on`, `deleted`) VALUES
(1, 1, 'logged in from: 192.168.2.89', 'users', '2013-06-07 11:29:53', 0),
(2, 1, 'Created Module: Category : 192.168.2.89', 'modulebuilder', '2013-06-07 11:34:37', 0),
(3, 1, 'Created Module: Book : 192.168.2.89', 'modulebuilder', '2013-06-07 11:41:20', 0),
(4, 1, 'logged in from: 192.168.2.89', 'users', '2013-06-10 05:03:20', 0),
(5, 1, 'Created record with ID: 1 : 192.168.2.89', 'book', '2013-06-10 05:26:19', 0),
(6, 1, 'Created record with ID: 1 : 192.168.2.89', 'category', '2013-06-10 05:33:31', 0),
(7, 1, 'Created record with ID: 2 : 192.168.2.89', 'book', '2013-06-10 05:50:48', 0),
(8, 1, 'Created record with ID: 3 : 192.168.2.89', 'book', '2013-06-10 05:52:19', 0),
(9, 1, 'Created record with ID: 4 : 192.168.2.89', 'book', '2013-06-10 05:54:24', 0),
(10, 1, 'Created record with ID: 5 : 192.168.2.89', 'book', '2013-06-10 05:58:36', 0),
(11, 1, 'Created record with ID: 6 : 192.168.2.89', 'book', '2013-06-10 05:58:58', 0),
(12, 1, 'Created record with ID: 7 : 192.168.2.89', 'book', '2013-06-10 05:59:06', 0),
(13, 1, 'Created record with ID: 8 : 192.168.2.89', 'book', '2013-06-10 06:40:00', 0),
(14, 1, 'Created record with ID: 9 : 192.168.2.89', 'book', '2013-06-10 06:47:50', 0),
(15, 1, 'Created record with ID: 2 : 192.168.2.89', 'category', '2013-06-10 06:52:27', 0),
(16, 1, 'Created record with ID: 10 : 192.168.2.89', 'book', '2013-06-10 06:52:57', 0),
(17, 1, 'Created record with ID: 11 : 192.168.2.89', 'book', '2013-06-10 07:05:42', 0),
(18, 1, 'Created record with ID: 12 : 192.168.2.89', 'book', '2013-06-10 09:29:22', 0),
(19, 1, 'Created record with ID: 3 : 192.168.2.89', 'category', '2013-06-10 09:49:34', 0),
(20, 1, 'Created record with ID: 4 : 192.168.2.89', 'category', '2013-06-10 09:50:04', 0),
(21, 1, 'logged out from: 192.168.2.89', 'users', '2013-06-10 11:13:20', 0),
(22, 1, 'logged in from: 192.168.2.89', 'users', '2013-06-11 03:49:00', 0),
(23, 1, 'Created record with ID: 13 : 192.168.2.89', 'book', '2013-06-11 03:51:05', 0),
(24, 1, 'Created record with ID: 14 : 192.168.2.89', 'book', '2013-06-11 04:46:36', 0),
(25, 1, 'Created record with ID: 15 : 192.168.2.89', 'book', '2013-06-11 04:47:42', 0),
(26, 1, 'Created record with ID: 16 : 192.168.2.89', 'book', '2013-06-11 04:48:34', 0),
(27, 1, 'Created record with ID: 5 : 192.168.2.89', 'category', '2013-06-11 07:09:12', 0),
(28, 1, 'logged in from: 192.168.2.89', 'users', '2013-06-11 09:35:59', 0),
(29, 1, 'Created record with ID: 17 : 192.168.2.89', 'book', '2013-06-11 09:37:27', 0),
(30, 1, 'logged in from: 192.168.2.89', 'users', '2013-06-13 05:46:32', 0),
(31, 1, 'logged out from: 192.168.2.89', 'users', '2013-06-13 05:48:33', 0),
(32, 1, 'logged in from: 192.168.2.89', 'users', '2013-06-13 05:54:27', 0),
(33, 1, 'logged out from: 192.168.2.89', 'users', '2013-06-13 05:57:58', 0),
(34, 1, 'logged in from: 192.168.2.89', 'users', '2013-06-13 06:12:21', 0),
(35, 1, 'Created record with ID: 1 : 192.168.2.89', 'book', '2013-06-13 06:18:45', 0),
(36, 1, 'Created record with ID: 2 : 192.168.2.89', 'book', '2013-06-13 06:25:46', 0),
(37, 1, 'Created record with ID: 3 : 192.168.2.89', 'book', '2013-06-13 06:34:24', 0),
(38, 1, 'Created record with ID: 4 : 192.168.2.89', 'book', '2013-06-13 06:38:37', 0),
(39, 1, 'Created record with ID: 5 : 192.168.2.89', 'book', '2013-06-13 06:41:00', 0),
(40, 1, 'Created record with ID: 6 : 192.168.2.89', 'category', '2013-06-13 06:46:57', 0),
(41, 1, 'Created record with ID: 7 : 192.168.2.89', 'category', '2013-06-13 06:47:22', 0),
(42, 1, 'Created record with ID: 8 : 192.168.2.89', 'category', '2013-06-13 06:47:45', 0),
(43, 1, 'Created record with ID: 9 : 192.168.2.89', 'category', '2013-06-13 06:48:12', 0),
(44, 1, 'Created record with ID: 10 : 192.168.2.89', 'category', '2013-06-13 06:48:59', 0),
(45, 1, 'Created record with ID: 11 : 192.168.2.89', 'category', '2013-06-13 06:50:53', 0),
(46, 1, 'Created record with ID: 12 : 192.168.2.89', 'category', '2013-06-13 06:51:11', 0),
(47, 1, 'Created record with ID: 13 : 192.168.2.89', 'category', '2013-06-13 06:51:37', 0),
(48, 1, 'Created record with ID: 6 : 192.168.2.89', 'book', '2013-06-13 07:09:12', 0),
(49, 1, 'Created record with ID: 7 : 192.168.2.89', 'book', '2013-06-13 07:13:33', 0),
(50, 1, 'Created record with ID: 8 : 192.168.2.89', 'book', '2013-06-13 07:15:47', 0),
(51, 1, 'Created record with ID: 9 : 192.168.2.89', 'book', '2013-06-13 07:18:01', 0),
(52, 1, 'Created record with ID: 10 : 192.168.2.89', 'book', '2013-06-13 07:20:11', 0),
(53, 1, 'logged in from: 192.168.2.89', 'users', '2013-06-13 09:59:51', 0),
(54, 1, 'Updated record with ID: 3 : 192.168.2.89', 'book', '2013-06-13 10:13:36', 0),
(55, 1, 'Created record with ID: 11 : 192.168.2.89', 'book', '2013-06-13 10:18:48', 0),
(56, 1, 'Updated record with ID: 11 : 192.168.2.89', 'book', '2013-06-13 10:19:30', 0),
(57, 1, 'Updated record with ID: 11 : 192.168.2.89', 'book', '2013-06-13 10:20:23', 0),
(58, 1, 'Created record with ID: 12 : 192.168.2.89', 'book', '2013-06-13 10:23:15', 0),
(59, 1, 'Updated record with ID: 12 : 192.168.2.89', 'book', '2013-06-13 10:23:41', 0),
(60, 1, 'Deleted record with ID: 12 : 192.168.2.89', 'book', '2013-06-13 10:26:27', 0),
(61, 1, 'Created record with ID: 13 : 192.168.2.89', 'book', '2013-06-13 10:27:03', 0),
(62, 1, 'Updated record with ID: 13 : 192.168.2.89', 'book', '2013-06-13 10:27:23', 0),
(63, 1, 'Created record with ID: 14 : 192.168.2.89', 'book', '2013-06-13 10:32:02', 0),
(64, 1, 'Updated record with ID: 14 : 192.168.2.89', 'book', '2013-06-13 10:32:25', 0),
(65, 1, 'Updated record with ID: 14 : 192.168.2.89', 'book', '2013-06-13 10:33:20', 0),
(66, 1, 'Updated record with ID: 14 : 192.168.2.89', 'book', '2013-06-13 10:33:44', 0),
(67, 1, 'logged out from: 192.168.2.89', 'users', '2013-06-13 10:36:27', 0),
(68, 1, 'logged in from: 192.168.2.89', 'users', '2013-06-13 10:37:10', 0),
(69, 1, 'logged out from: 192.168.2.89', 'users', '2013-06-13 10:59:37', 0),
(70, 1, 'logged in from: 192.168.2.89', 'users', '2013-06-14 03:57:25', 0),
(71, 1, 'logged out from: 192.168.2.89', 'users', '2013-06-14 04:26:31', 0),
(72, 1, 'logged in from: 192.168.2.89', 'users', '2013-06-14 04:51:03', 0),
(73, 1, 'Created record with ID: 1 : 192.168.2.89', 'book', '2013-06-14 05:26:03', 0),
(74, 1, 'Created record with ID: 2 : 192.168.2.89', 'book', '2013-06-14 05:33:28', 0),
(75, 1, 'logged in from: 192.168.2.89', 'users', '2013-06-14 08:27:21', 0),
(76, 1, 'Created record with ID: 3 : 192.168.2.89', 'book', '2013-06-14 08:59:20', 0),
(77, 1, 'Created record with ID: 4 : 192.168.2.89', 'book', '2013-06-14 09:04:02', 0),
(78, 1, 'Created record with ID: 5 : 192.168.2.89', 'book', '2013-06-14 09:05:29', 0),
(79, 1, 'Updated record with ID: 1 : 192.168.2.89', 'book', '2013-06-14 09:19:40', 0),
(80, 1, 'Created record with ID: 6 : 192.168.2.89', 'book', '2013-06-14 10:26:12', 0),
(81, 1, 'Created record with ID: 7 : 192.168.2.89', 'book', '2013-06-14 10:27:06', 0),
(82, 1, 'Created record with ID: 8 : 192.168.2.89', 'book', '2013-06-14 11:49:04', 0),
(83, 1, 'logged in from: 192.168.2.89', 'users', '2013-06-17 04:39:17', 0),
(84, 1, 'Updated record with ID: 8 : 192.168.2.89', 'book', '2013-06-17 05:13:01', 0),
(85, 1, 'Updated record with ID: 7 : 192.168.2.89', 'book', '2013-06-17 06:12:04', 0);

CREATE TABLE IF NOT EXISTS `es_book` (
  `book_id` int(11) NOT NULL AUTO_INCREMENT,
  `book_book_title` varchar(255) NOT NULL,
  `book_book_description` text NOT NULL,
  `book_book_file` varchar(150) DEFAULT NULL,
  `book_external_link` text NOT NULL,
  `book_file_title` varchar(255) DEFAULT NULL,
  `book_file_size` varchar(50) DEFAULT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`book_id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

CREATE TABLE IF NOT EXISTS `es_category` (
  `category_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(100) NOT NULL,
  `category_category_description` text NOT NULL,
  `modified_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

INSERT INTO `es_category` (`category_id`, `category_name`, `category_category_description`, `modified_on`) VALUES
(1, 'Uncategorize', '<p>Uncategorize</p>', '0000-00-00 00:00:00'),
(2, 'Science & Technology', '<p>Science &amp; Technology</p>', '0000-00-00 00:00:00'),
(3, 'Health', '<p>Healthy</p>', '0000-00-00 00:00:00'),
(4, 'Bussines', '<p>Bussiness</p>', '0000-00-00 00:00:00'),
(5, 'Tutorial', '<p>Tutorial Books</p>', '0000-00-00 00:00:00'),
(6, 'Social Media', '<p>Contents about Social Media</p>', '0000-00-00 00:00:00'),
(7, 'Fiction', '<p>Fiction Book</p>', '0000-00-00 00:00:00'),
(8, 'Finance', '<p>Finance Book</p>', '0000-00-00 00:00:00'),
(9, 'Bussiness And Marketing', '<p>book</p>', '0000-00-00 00:00:00'),
(10, 'Art and Design', '<p>Art</p>', '0000-00-00 00:00:00'),
(11, 'News', '<p>News</p>', '0000-00-00 00:00:00'),
(12, 'Magazines', '<p>Magz</p>', '0000-00-00 00:00:00'),
(13, 'Sports', '<p>Sports</p>', '0000-00-00 00:00:00');

CREATE TABLE IF NOT EXISTS `es_email_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `to_email` varchar(128) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `alt_message` text,
  `max_attempts` int(11) NOT NULL DEFAULT '3',
  `attempts` int(11) NOT NULL DEFAULT '0',
  `success` tinyint(1) NOT NULL DEFAULT '0',
  `date_published` datetime DEFAULT NULL,
  `last_attempt` datetime DEFAULT NULL,
  `date_sent` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `es_login_attempts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(40) NOT NULL,
  `login` varchar(50) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `es_permissions` (
  `permission_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(100) NOT NULL,
  `status` enum('active','inactive','deleted') DEFAULT 'active',
  PRIMARY KEY (`permission_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=59 ;

INSERT INTO `es_permissions` (`permission_id`, `name`, `description`, `status`) VALUES
(1, 'Site.Signin.Allow', 'Allow users to login to the site', 'active'),
(2, 'Site.Content.View', 'Allow users to view the Content Context', 'active'),
(3, 'Site.Reports.View', 'Allow users to view the Reports Context', 'active'),
(4, 'Site.Settings.View', 'Allow users to view the Settings Context', 'active'),
(5, 'Site.Developer.View', 'Allow users to view the Developer Context', 'active'),
(6, 'Bonfire.Roles.Manage', 'Allow users to manage the user Roles', 'active'),
(7, 'Bonfire.Users.Manage', 'Allow users to manage the site Users', 'active'),
(8, 'Bonfire.Users.View', 'Allow users access to the User Settings', 'active'),
(9, 'Bonfire.Users.Add', 'Allow users to add new Users', 'active'),
(10, 'Bonfire.Database.Manage', 'Allow users to manage the Database settings', 'active'),
(11, 'Bonfire.Emailer.Manage', 'Allow users to manage the Emailer settings', 'active'),
(12, 'Bonfire.Logs.View', 'Allow users access to the Log details', 'active'),
(13, 'Bonfire.Logs.Manage', 'Allow users to manage the Log files', 'active'),
(14, 'Bonfire.Emailer.View', 'Allow users access to the Emailer settings', 'active'),
(15, 'Site.Signin.Offline', 'Allow users to login to the site when the site is offline', 'active'),
(16, 'Bonfire.Permissions.View', 'Allow access to view the Permissions menu unders Settings Context', 'active'),
(17, 'Bonfire.Permissions.Manage', 'Allow access to manage the Permissions in the system', 'active'),
(18, 'Bonfire.Roles.Delete', 'Allow users to delete user Roles', 'active'),
(19, 'Bonfire.Modules.Add', 'Allow creation of modules with the builder.', 'active'),
(20, 'Bonfire.Modules.Delete', 'Allow deletion of modules.', 'active'),
(21, 'Permissions.Administrator.Manage', 'To manage the access control permissions for the Administrator role.', 'active'),
(22, 'Permissions.Editor.Manage', 'To manage the access control permissions for the Editor role.', 'active'),
(24, 'Permissions.User.Manage', 'To manage the access control permissions for the User role.', 'active'),
(25, 'Permissions.Developer.Manage', 'To manage the access control permissions for the Developer role.', 'active'),
(27, 'Activities.Own.View', 'To view the users own activity logs', 'active'),
(28, 'Activities.Own.Delete', 'To delete the users own activity logs', 'active'),
(29, 'Activities.User.View', 'To view the user activity logs', 'active'),
(30, 'Activities.User.Delete', 'To delete the user activity logs, except own', 'active'),
(31, 'Activities.Module.View', 'To view the module activity logs', 'active'),
(32, 'Activities.Module.Delete', 'To delete the module activity logs', 'active'),
(33, 'Activities.Date.View', 'To view the users own activity logs', 'active'),
(34, 'Activities.Date.Delete', 'To delete the dated activity logs', 'active'),
(35, 'Bonfire.UI.Manage', 'Manage the Bonfire UI settings', 'active'),
(36, 'Bonfire.Settings.View', 'To view the site settings page.', 'active'),
(37, 'Bonfire.Settings.Manage', 'To manage the site settings.', 'active'),
(38, 'Bonfire.Activities.View', 'To view the Activities menu.', 'active'),
(39, 'Bonfire.Database.View', 'To view the Database menu.', 'active'),
(40, 'Bonfire.Migrations.View', 'To view the Migrations menu.', 'active'),
(41, 'Bonfire.Builder.View', 'To view the Modulebuilder menu.', 'active'),
(42, 'Bonfire.Roles.View', 'To view the Roles menu.', 'active'),
(43, 'Bonfire.Sysinfo.View', 'To view the System Information page.', 'active'),
(44, 'Bonfire.Translate.Manage', 'To manage the Language Translation.', 'active'),
(45, 'Bonfire.Translate.View', 'To view the Language Translate menu.', 'active'),
(46, 'Bonfire.UI.View', 'To view the UI/Keyboard Shortcut menu.', 'active'),
(47, 'Bonfire.Update.Manage', 'To manage the Bonfire Update.', 'active'),
(48, 'Bonfire.Update.View', 'To view the Developer Update menu.', 'active'),
(49, 'Bonfire.Profiler.View', 'To view the Console Profiler Bar.', 'active'),
(50, 'Bonfire.Roles.Add', 'To add New Roles', 'active'),
(51, 'Category.Content.View', '', 'active'),
(52, 'Category.Content.Create', '', 'active'),
(53, 'Category.Content.Edit', '', 'active'),
(54, 'Category.Content.Delete', '', 'active'),
(55, 'Book.Content.View', '', 'active'),
(56, 'Book.Content.Create', '', 'active'),
(57, 'Book.Content.Edit', '', 'active'),
(58, 'Book.Content.Delete', '', 'active');

CREATE TABLE IF NOT EXISTS `es_roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(60) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `default` tinyint(1) NOT NULL DEFAULT '0',
  `can_delete` tinyint(1) NOT NULL DEFAULT '1',
  `login_destination` varchar(255) NOT NULL DEFAULT '/',
  `deleted` int(1) NOT NULL DEFAULT '0',
  `default_context` varchar(255) NOT NULL DEFAULT 'content',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

INSERT INTO `es_roles` (`role_id`, `role_name`, `description`, `default`, `can_delete`, `login_destination`, `deleted`, `default_context`) VALUES
(1, 'Administrator', 'Has full control over every aspect of the site.', 0, 0, '', 0, 'content'),
(2, 'Editor', 'Can handle day-to-day management, but does not have full power.', 0, 1, '', 0, 'content'),
(4, 'User', 'This is the default user with access to login.', 1, 0, '', 0, 'content'),
(6, 'Developer', 'Developers typically are the only ones that can access the developer tools. Otherwise identical to Administrators, at least until the site is handed off.', 0, 1, '', 0, 'content');

CREATE TABLE IF NOT EXISTS `es_role_permissions` (
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`role_id`,`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `es_role_permissions` (`role_id`, `permission_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 15),
(1, 16),
(1, 17),
(1, 18),
(1, 19),
(1, 20),
(1, 21),
(1, 22),
(1, 24),
(1, 25),
(1, 27),
(1, 28),
(1, 29),
(1, 30),
(1, 31),
(1, 32),
(1, 33),
(1, 34),
(1, 35),
(1, 36),
(1, 37),
(1, 38),
(1, 39),
(1, 40),
(1, 41),
(1, 42),
(1, 43),
(1, 44),
(1, 45),
(1, 46),
(1, 47),
(1, 48),
(1, 49),
(1, 50),
(1, 51),
(1, 52),
(1, 53),
(1, 54),
(1, 55),
(1, 56),
(1, 57),
(1, 58),
(2, 1),
(2, 2),
(2, 3),
(4, 1),
(6, 1),
(6, 2),
(6, 3),
(6, 4),
(6, 5),
(6, 6),
(6, 7),
(6, 8),
(6, 9),
(6, 10),
(6, 11),
(6, 12),
(6, 13),
(6, 49);

CREATE TABLE IF NOT EXISTS `es_schema_version` (
  `type` varchar(40) NOT NULL,
  `version` int(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `es_schema_version` (`type`, `version`) VALUES
('app_', 0),
('book_', 2),
('category_', 2),
('core', 34);

CREATE TABLE IF NOT EXISTS `es_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `es_settings` (
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `module` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `unique - name` (`name`),
  KEY `index - name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `es_settings` (`name`, `module`, `value`) VALUES
('auth.allow_name_change', 'core', '1'),
('auth.allow_register', 'core', '1'),
('auth.allow_remember', 'core', '1'),
('auth.do_login_redirect', 'core', '1'),
('auth.login_type', 'core', 'email'),
('auth.name_change_frequency', 'core', '1'),
('auth.name_change_limit', 'core', '1'),
('auth.password_force_mixed_case', 'core', '0'),
('auth.password_force_numbers', 'core', '0'),
('auth.password_force_symbols', 'core', '0'),
('auth.password_min_length', 'core', '8'),
('auth.password_show_labels', 'core', '0'),
('auth.remember_length', 'core', '1209600'),
('auth.use_extended_profile', 'core', '0'),
('auth.use_usernames', 'core', '1'),
('auth.user_activation_method', 'core', '0'),
('form_save', 'core.ui', 'ctrl+s/⌘+s'),
('goto_content', 'core.ui', 'alt+c'),
('mailpath', 'email', '/usr/sbin/sendmail'),
('mailtype', 'email', 'text'),
('protocol', 'email', 'mail'),
('sender_email', 'email', 'admin@android.com'),
('site.languages', 'core', 'a:3:{i:0;s:7:"english";i:1;s:10:"portuguese";i:2;s:7:"persian";}'),
('site.list_limit', 'core', '25'),
('site.show_front_profiler', 'core', '1'),
('site.show_profiler', 'core', '1'),
('site.status', 'core', '1'),
('site.system_email', 'core', 'admin@android.com'),
('site.title', 'core', 'Android Basic eBook'),
('smtp_host', 'email', ''),
('smtp_pass', 'email', ''),
('smtp_port', 'email', ''),
('smtp_timeout', 'email', ''),
('smtp_user', 'email', ''),
('updates.bleeding_edge', 'core', '1'),
('updates.do_check', 'core', '1');

CREATE TABLE IF NOT EXISTS `es_users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL DEFAULT '4',
  `email` varchar(120) NOT NULL,
  `username` varchar(30) NOT NULL DEFAULT '',
  `password_hash` varchar(40) NOT NULL,
  `reset_hash` varchar(40) DEFAULT NULL,
  `salt` varchar(7) NOT NULL,
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_ip` varchar(40) NOT NULL DEFAULT '',
  `created_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `ban_message` varchar(255) DEFAULT NULL,
  `reset_by` int(10) DEFAULT NULL,
  `display_name` varchar(255) DEFAULT '',
  `display_name_changed` date DEFAULT NULL,
  `timezone` char(4) NOT NULL DEFAULT 'UM6',
  `language` varchar(20) NOT NULL DEFAULT 'english',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `activate_hash` varchar(40) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

INSERT INTO `es_users` (`id`, `role_id`, `email`, `username`, `password_hash`, `reset_hash`, `salt`, `last_login`, `last_ip`, `created_on`, `deleted`, `banned`, `ban_message`, `reset_by`, `display_name`, `display_name_changed`, `timezone`, `language`, `active`, `activate_hash`) VALUES
(1, 1, 'admin@android.com', 'admin', 'a0186bfa3d4c508809bf85130fe698ae5d55a39a', NULL, 'fE2Sbnd', '2013-06-17 04:39:17', '192.168.2.89', '0000-00-00 00:00:00', 0, 0, NULL, NULL, '', NULL, 'UM6', 'english', 1, '');

CREATE TABLE IF NOT EXISTS `es_user_cookies` (
  `user_id` bigint(20) NOT NULL,
  `token` varchar(128) NOT NULL,
  `created_on` datetime NOT NULL,
  KEY `token` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `es_user_meta` (
  `meta_id` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) NOT NULL DEFAULT '',
  `meta_value` text,
  PRIMARY KEY (`meta_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;


ALTER TABLE `es_book`
  ADD CONSTRAINT `es_book_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `es_category` (`category_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
